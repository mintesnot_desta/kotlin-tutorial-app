package com.example.intent;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.android.material.navigation.NavigationView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Home extends Fragment implements View.OnClickListener{
    Button start1,start2,start3,start4;
    NavigationView navigationView;
    DrawerLayout drawerLayout;
    FrameLayout frameLayout;
    ProgressBar progressBar;
    ImageView imageView1,imageView2,imageView3,imageView4;
    View view;


    public Home(Context context) {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view= inflater.inflate(R.layout.home_fragment, container, false);
        start1=view.findViewById(R.id.beginer_start);
        start2=view.findViewById(R.id.intermidiate_start);
        start3=view.findViewById(R.id.advanced_start);
        start4=view.findViewById(R.id.ninja_start);
        start1.setOnClickListener(this);
        return  view;
    }

    @Override
    public void onClick(View view) {
        Lesson_Fragment lesson_fragment=new Lesson_Fragment(getContext());
        Intent intent=new Intent(getContext(),DetailActivity.class);
        Lesson lesson;
        switch (view.getId())
        {

            case R.id.beginer_start:
                lesson=(Lesson)lesson_fragment.list.get(0);
                intent.putExtra("title",lesson.getTitle());
                intent.putExtra("detail",lesson.getDetails());
              startActivity(intent);
              break;
            case R.id.advanced_start:
                 lesson=(Lesson)lesson_fragment.list.get(5);
                intent.putExtra("title",lesson.getTitle());
                intent.putExtra("detail",lesson.getDetails());
                startActivity(intent);
                break;

        }
    }
}
