package com.example.intent;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    NavigationView navigationView;
    DrawerLayout drawerLayout;
    FrameLayout frameLayout;
    List list1,list2;
    int code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        frameLayout=findViewById(R.id.frame);
        navigationView=findViewById(R.id.navigation_view);
        drawerLayout=findViewById(R.id.drawer);
        list1=new ArrayList();
        list2=new ArrayList();
        getQuestion();
        ActionBar actionBar=getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
        Home home=new Home(getApplicationContext());
        Fragment_changer(home);
        Intent intent=getIntent();
         code=intent.getIntExtra("code",1);
         if(code==7)
         {
            // onWindowFocusChanged(true);
         }
         else {
             //onWindowFocusChanged(false);
         }
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId())
                {
                    case R.id.home:
                        Home home=new Home(getApplicationContext());
                        Fragment_changer(home);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.content:
                        Lesson_Fragment lesson_fragment=new Lesson_Fragment(getApplicationContext());
                        Fragment_changer(lesson_fragment);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.quiz:
                        Quiz quiz=new Quiz(list1,list2);
                        Fragment_changer(quiz);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.fullscreen:
                        Tools tools=new Tools();
                        Fragment_changer(tools);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.nightmode:

                        drawerLayout.closeDrawers();
                        break;
                    case R.id.news:
                        News news=new News();
                        Fragment_changer(news);
                        drawerLayout.closeDrawers();
                        break;
                }
                return false;
            }
        });
    }
    public void Fragment_changer(Fragment fragment)
    {
        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        frameLayout.removeAllViews();
        fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frame,fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }
    public void getQuestion()
    {
        list1.add(new Question(1,"What Is Kotlin?","\n" +
                "It is an open source programming language that combines object-oriented programming features.",
                "The features like Range Expression, Extension Function," +
                        " Companion Object, Smart casts, Data classes are considered to be surplus of the Kotlin Language."));
        list1.add(new Question(2," Which Type Of Programming Does Kotlin Support","\n" +
                "\n" + "Kotlin supports only two types of programming, and they are:",
                "Procedural programming\n" + "Object-oriented programming" +
                        " Companion Object, Smart casts, Data classes are considered to be surplus of the Kotlin Language."));
        list1.add(new Question(3," Why Is Kotlin Preferred Over Java?","\n" +
                "\n" +
                "Kotlin eases the coding process as it is simpler than Java and has many features required, that is not " +
                "provided by Java yet like Extension functions, Null Safety, range expressions etc.",
                "In Kotlin, we code approximately 40% less number of code lines as compared with Java."));
        list1.add(new Question(4,"What Are The Different Types Of Constructors In Kotlin?","\n" +
                "There are two types of constructors in Kotlin:",
                "Primary constructor: It is a section of the Class header and is declared after the class name.\n" +
                        "Secondary constructor: This constructor is declared inside the body.\n" +
                        "Note: There can be more secondary constructors for a class."));
        list1.add(new Question(5,"Can You Execute Kotlin Code Without Jvm?","\n" +
                "\n" +
                "JVM, which stands for Java Virtual Machine is a feature of Kotlin." +
                "This feature compiles a Kotlin code into a native code, which can be done without JVM too.",
                "The features like Range Expression, Extension Function," +
                        " Companion Object, Smart casts, Data classes are considered to be surplus of the Kotlin Language."));
        list1.add(new Question(6," What Are The Modifiers That Are Available In Kotlin?","\n" +
                "\n" +
                "The modifier in Kotlin provides the developer to customize the declarations as per the requirements.\n" +
                "\n" +
                "Kotlin provides four modifiers.",
                "They are:\n" +
                        "\n" +
                        "Private: This makes the declaration visible only inside the file containing declaration.\n" +
                        "Public: It is by default, which means that the declarations will be visible everywhere.\n" +
                        "Internal: This makes the declaration visible everywhere in the same modules.\n" +
                        "Protected: This keeps the declaration protected and is not available for top-level declarations."));
        list2.add(new Choices(1,"scripting language","the same with java","used to style web",
                "It is an open source programming language that combines object-oriented programming features."));
        list2.add(new Choices(2,"scripting language","the same with java","used to style web",
                "It is an open source programming language that combines object-oriented programming features."));
        list2.add(new Choices(3,"scripting language","the same with java","used to style web",
                "It is an open source programming language that combines object-oriented programming features."));
        list2.add(new Choices(4,"scripting language","the same with java","used to style web",
                "It is an open source programming language that combines object-oriented programming features."));
        list2.add(new Choices(5,"scripting language","the same with java","used to style web",
                "It is an open source programming language that combines object-oriented programming features."));
        list2.add(new Choices(6,"scripting language","the same with java","used to style web",
                "It is an open source programming language that combines object-oriented programming features."));
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    // Shows the system bars by removing all the flags
// except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }
}
