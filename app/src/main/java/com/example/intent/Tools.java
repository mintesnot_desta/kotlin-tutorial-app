package com.example.intent;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;


/**
 * A simple {@link Fragment} subclass.
 */
public class Tools extends Fragment {
  Switch aSwitch;
  View view;

    public Tools() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_tools, container, false);
        aSwitch=view.findViewById(R.id.fullscreen);
        if (aSwitch.isActivated())
        {
            Intent intent=new Intent(getContext(),MainActivity.class);
            intent.putExtra("code",7);
            startActivity(intent);
        }

        return view;
    }



}
