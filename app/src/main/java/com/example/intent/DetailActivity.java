package com.example.intent;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DetailActivity extends AppCompatActivity {
   TextView title,detail;
   TextToSpeech textToSpeech;
   Button speak,nextchapter;
   List list;
   int index=1;
    Lesson lesson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        title=findViewById(R.id.lesson);
        detail=findViewById(R.id.detail);
        speak=findViewById(R.id.speak);
        nextchapter=findViewById(R.id.nextchapter);
        list=new ArrayList();
        list.add(new Lesson(1,"Hello World",Data.helloworld,"htttp://www.hello world"));
        list.add(new Lesson(2,"basics of kotlin",Data.basics,"htttp://www.hello world"));
        list.add(new Lesson(3,"reading from cmd",Data.readingcmd,"htttp://www.hello world"));
        list.add(new Lesson(4,"string literals",Data.string,"htttp://www.hello world"));
        list.add(new Lesson(5,"loops in kotlin",Data.loops,"htttp://www.hello world"));
        list.add(new Lesson(6,"conditional statement",Data.conditionalstatement,"htttp://www.hello world"));
        list.add(new Lesson(7,"Array Iteration in kotlin",Data.arrayIteration,"htttp://www.hello world"));
        list.add(new Lesson(8," basics of function in kotlin",Data.function,"htttp://www.hello world"));
        list.add(new Lesson(9,"exceptions in kotlin",Data.exception,"htttp://www.hello world"));
        list.add(new Lesson(10,"Collection in kotlin",Data.collection,"htttp://www.hello world"));
        list.add(new Lesson(11,"Range in kotlin",Data.range,"htttp://www.hello world"));
        list.add(new Lesson(12,"Regex in kotlin",Data.regex,"htttp://www.hello world"));
        list.add(new Lesson(13," how to use RecyclerView with kotlin",Data.recyclerview,"htttp://www.hello world"));
        list.add(new Lesson(14,"Kotlin and java interoperatability",Data.javakotlin,"htttp://www.hello world"));

        nextchapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(index<list.size())
                {
                  lesson =(Lesson) list.get(index);
                  title.setText(lesson.getTitle());
                  detail.setText(lesson.getDetails());
                      index++;
                }

            }
        });
        if(getIntent().getStringExtra("title")!=null && getIntent().getStringExtra("detail")!=null)
        {
            title.setText(getIntent().getStringExtra("title"));
            detail.setText(getIntent().getStringExtra("detail"));
        }
        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int ttsLang = textToSpeech.setLanguage(Locale.US);

                    if (ttsLang == TextToSpeech.LANG_MISSING_DATA
                            || ttsLang == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Toast.makeText(getApplicationContext(),"The Language is not supported!",Toast.LENGTH_LONG);
                       // Log.e("TTS", "The Language is not supported!");
                    } else {
                        Toast.makeText(getApplicationContext(),"Language Supported.",Toast.LENGTH_LONG);
                    }
                    Toast.makeText(getApplicationContext(),"Initialization success",Toast.LENGTH_LONG);
                } else {
                    Toast.makeText(getApplicationContext(), "TTS Initialization failed!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        speak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
           start_speech();
            }
        });
    }
    public  void start_speech()
    {
        String data = detail.getText().toString();
        int speechStatus = textToSpeech.speak(data, TextToSpeech.QUEUE_FLUSH, null);
        if (speechStatus == TextToSpeech.ERROR) {
            Toast.makeText(getApplicationContext(),"error!",Toast.LENGTH_LONG);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        textToSpeech.shutdown();
    }
}
