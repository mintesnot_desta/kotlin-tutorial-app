package com.example.intent;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Quiz extends Fragment implements View.OnClickListener {
    List questionlist,answerlist;
    Button prev,next,describe;
    TextView qno,score,question,description;
    ProgressBar progressBar;
    RadioButton a,b,c,d;
    View view;
    int index=0,scoren=0;
    RadioGroup radioGroup;
    Question current_question;
    Choices choices;
    Dialog dialog;
    public Quiz(List questionlist,List answerlist) {
        // Required empty public constructor
        this.questionlist=questionlist;
        this.answerlist=answerlist;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_quiz, container, false);
        prev=view.findViewById(R.id.prev);
        next=view.findViewById(R.id.next);
        describe=view.findViewById(R.id.describe);
        prev.setOnClickListener(this);
        next.setOnClickListener(this);
        describe.setOnClickListener(this);
        describe.setClickable(false);
        progressBar=view.findViewById(R.id.progressBar);
        progressBar.setMax(6);
        progressBar.setProgress(index);
        radioGroup=view.findViewById(R.id.rgroup);
        a=view.findViewById(R.id.a);
        b=view.findViewById(R.id.b);
        c=view.findViewById(R.id.c);
        d=view.findViewById(R.id.d);
        qno=view.findViewById(R.id.qnumber);
        score=view.findViewById(R.id.score);
        question=view.findViewById(R.id.question);
        description=view.findViewById(R.id.description);
        current_question=(Question)questionlist.get(index);
       choices=(Choices)answerlist.get(current_question.qno);
        question.setText(current_question.getQuestion());
        a.setText(choices.a);
        b.setText(choices.b);
        c.setText(choices.c);
        d.setText(current_question.getAnswer());
        qno.setText("Qno="+current_question.getQno());
        score.setText("Score="+0);
        return view;
    }
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.prev:
                index=index-1;
                if (index>=0 || index<=questionlist.size()) {
                    setData();
                }
                break;
            case R.id.next:
                switch (radioGroup.getCheckedRadioButtonId())
                {
                    case R.id.a:
                        try {
                            Check_Answer(a);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        break;
                    case R.id.b:
                        try {
                            Check_Answer(b);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        break;
                    case R.id.c:
                        try {
                            Check_Answer(c);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        break;
                    case R.id.d:
                        try {
                            Check_Answer(d);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        break;
                }

                if (index>=0 && index<questionlist.size()) {
                    setData();
                }
                else if(index==questionlist.size())
                {
                    ShowResult();

                }
                index=index+1;
                break;

            case R.id.describe:
                description.setText(current_question.getDescription());
                break;
        }

    }
    public void setData()
    {
        if (index<questionlist.size() && index>=0) {
            progressBar.setProgress(index);
            current_question = (Question) questionlist.get(index);
            choices = (Choices) answerlist.get(current_question.qno-1);
            question.setText(current_question.getQuestion());
            a.setText(choices.a);
            b.setText(choices.b);
            c.setText(choices.c);
            d.setText(current_question.getAnswer());
            qno.setText("Qno=" + current_question.getQno());
            score.setText("Score=" + scoren);

        }

    }
    public void Check_Answer(RadioButton rb) throws InterruptedException {
        String user_choice=rb.getText().toString();
        if(user_choice.equals(current_question.answer))
        {
           // Toast.makeText(getContext(),"Correct",Toast.LENGTH_SHORT).show();
            showDialog(1);
            scoren=scoren+1;
            describe.setClickable(true);
        }
        else
        {
            showDialog(0);
            describe.setClickable(true);
        }

    }
    public void showDialog(int code) throws InterruptedException {
        if (code == 1) {
            dialog = new Dialog(getContext());
            dialog.setContentView(R.layout.popup);
            dialog.show();
           // Thread.sleep(2000);
            //dialog.dismiss();

        }
        else
            {
                dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.popupw);
                dialog.show();
               // Thread.sleep(2000);
                //dialog.dismiss();

            }
        //Thread.sleep(2000);
        //dialog.dismiss();
    }
    public  void ShowResult()
    {
        dialog=new Dialog(getContext());
        dialog.setContentView(R.layout.result);
        TextView total=dialog.findViewById(R.id.totalquestion);
        TextView answered=dialog.findViewById(R.id.answered);
        total.setText(""+questionlist.size());
        answered.setText(""+scoren);
        dialog.show();
        Button close=dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent=new Intent(getContext(),MainActivity.class);
                startActivity(intent);
            }
        });

    }

}
