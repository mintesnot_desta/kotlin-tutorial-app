package com.example.intent;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class LessonAdapter extends RecyclerView.Adapter<LessonAdapter.LessonViewHolder>
{
    Context context;
    List list;
    public LessonAdapter(Context context,List list)
    {
        this.context=context;
        this.list=list;
    }
    @NonNull
    @Override
    public LessonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view= LayoutInflater.from(context).inflate(R.layout.single_lesson,null);
        return new LessonViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull LessonViewHolder holder, int position)
    {
     Lesson lesson=(Lesson) list.get(position);
     holder.title.setText(lesson.getTitle());
     holder.progress.setText("35%");
    holder.progressBar.setProgress(34);
    holder.progressBar.setMax(100);
    }
    @Override
    public int getItemCount()
    {
        return list.size();
    }
    class LessonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener
    {
        TextView title,progress;
        ProgressBar progressBar;

        public LessonViewHolder(@NonNull View itemView)
        {
            super(itemView);
            progressBar=itemView.findViewById(R.id.progressBar1);
            title=itemView.findViewById(R.id.title);
            progress=itemView.findViewById(R.id.progress);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view)
        {
            Lesson lesson=(Lesson)list.get(getAdapterPosition());
            Intent intent=new Intent(context,DetailActivity.class);
            intent.putExtra("title",lesson.getTitle());
            intent.putExtra("detail",lesson.getDetails());
            context.startActivity(intent);
        }

        @Override
        public boolean onLongClick(View view) {
            list.remove(getAdapterPosition());
            return false;
        }
    }


}
