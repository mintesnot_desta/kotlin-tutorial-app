package com.example.intent;

public class Data {
    static  String arrayIteration="You can print the array elements using the loop same as the Java enhanced loop, but you need to change keyword\n" +
            "from : to in.\n" +
            "val asc = Array(5, { i -> (i * i).toString() })\n" +
            "for(s : String in asc){\n" +
            " println(s);\n" +
            "}\n" +
            "You can also change data type in for loop.\n" +
            "val asc = Array(5, { i -> (i * i).toString() })\n" +
            "for(s in asc){\n" +
            " println(s);\n" +
            "}";
    static String helloworld="All Kotlin programs start at the main function. Here is an example of a simple Kotlin \"Hello World\" program:\n" +
            "package my.program\n" +
            "fun main(args: Array) {\n" +
            " println(\"Hello, world!\")\n" +
            "}\n" +
            "Place the above code into a file named Main.kt (this filename is entirely arbitrary)\n" +
            "When targeting the JVM, the function will be compiled as a static method in a class with a name derived from the\n" +
            "filename. In the above example, the main class to run would be my.program.MainKt.\n" +
            "To change the name of the class that contains top-level functions for a particular file, place the following annotation\n" +
            "at the top of the file above the package statement:\n" +
            "@file:JvmName(\"MyApp\")\n" +
            "In this example, the main class to run would now be my.program.MyApp.\n" +
            "See also:\n" +
            "Package level functions including @JvmName annotation.\n" +
            "Annotation use-site targets\n" +
            "Section 1.2: Hello World using a Companion Object\n" +
            "Similar to using an Object Declaration, you can define the main function of a Kotlin program using a Companion\n" +
            "Object of a class.\n" +
            "package my.program\n" +
            "class App {\n" +
            " companion object {\n" +
            " @JvmStatic fun main(args: Array<string>) {\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 3\n" +
            " println(\"Hello World\")\n" +
            " }\n" +
            " }\n" +
            "}\n" +
            "The class name that you will run is the name of your class, in this case is my.program.App.\n" +
            "The advantage to this method over a top-level function is that the class name to run is more self-evident, and any\n" +
            "other functions you add are scoped into the class App. This is similar to the Object Declaration example, other\n" +
            "than you are in control of instantiating any classes to do further work.\n" +
            "A slight variation that instantiates the class to do the actual \"hello\":\n" +
            "class App {\n" +
            " companion object {\n" +
            " @JvmStatic fun main(args: Array<string>) {\n" +
            " App().run()\n" +
            " }\n" +
            " }\n" +
            " fun run() {\n" +
            " println(\"Hello World\")\n" +
            " }\n" +
            "}\n" +
            "See also:\n" +
            "Static Methods including the @JvmStatic annotation\n" +
            "Section 1.3: Hello World using an Object Declaration\n" +
            "You can alternatively use an Object Declaration that contains the main function for a Kotlin program.\n" +
            "package my.program\n" +
            "object App {\n" +
            " @JvmStatic fun main(args: Array) {\n" +
            " println(\"Hello World\")\n" +
            " }\n" +
            "}\n" +
            "The class name that you will run is the name of your object, in this case is my.program.App.\n" +
            "The advantage to this method over a top-level function is that the class name to run is more self-evident, and any\n" +
            "other functions you add are scoped into the class App. You then also have a singleton instance of App to store state\n" +
            "and do other work.\n" +
            "See also:\n" +
            "Static Methods including the @JvmStatic annotation";
    static  String array="These types do not inherit from Array<T> to avoid boxing, however, they have the same attributes and methods.\n" +
            "Kotlin type Factory function JVM type\n" +
            "BooleanArray booleanArrayOf(true, false) boolean[]\n" +
            "ByteArray byteArrayOf(1, 2, 3) byte[]\n" +
            "CharArray charArrayOf('a', 'b', 'c') char[]\n" +
            "DoubleArray doubleArrayOf(1.2, 5.0) double[]\n" +
            "FloatArray floatArrayOf(1.2, 5.0) float[]\n" +
            "IntArray intArrayOf(1, 2, 3) int[]\n" +
            "LongArray longArrayOf(1, 2, 3) long[]\n" +
            "ShortArray shortArrayOf(1, 2, 3) short[]\n" +
            "Section 4.3: Create an array\n" +
            "val a = arrayOf(1, 2, 3) // creates an Array<Int> of size 3 containing [1, 2, 3].\n" +
            "Section 4.4: Create an array using a closure\n" +
            "val a = Array(3) { i -> i * 2 } // creates an Array<Int> of size 3 containing [0, 2, 4]\n" +
            "Section 4.5: Create an uninitialized array\n" +
            "val a = arrayOfNulls<Int>(3) // creates an Array<Int?> of [null, null, null]\n" +
            "The returned array will always have a nullable type. Arrays of non-nullable items can't be created uninitialized.\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 9\n" +
            "Section 4.6: Extensions\n" +
            "average() is defined for Byte, Int, Long, Short, Double, Float and always returns Double:\n" +
            "val doubles = doubleArrayOf(1.5, 3.0)\n" +
            "print(doubles.average()) // prints 2.25\n" +
            "val ints = intArrayOf(1, 4)\n" +
            "println(ints.average()) // prints 2.5\n" +
            "component1(), component2(), ... component5() return an item of the array\n" +
            "getOrNull(index: Int) returns null if index is out of bounds, otherwise an item of the array\n" +
            "first(), last()\n" +
            "toHashSet() returns a HashSet<T> of all elements\n" +
            "sortedArray(), sortedArrayDescending() creates and returns a new array with sorted elements of current\n" +
            "sort(), sortDescending sort the array in-place\n" +
            "min(), max()\n";
    static  String collection="Chapter 5: Collections\n" +
            "Unlike many languages, Kotlin distinguishes between mutable and immutable collections (lists, sets, maps, etc).\n" +
            "Precise control over exactly when collections can be edited is useful for eliminating bugs, and for designing good\n" +
            "APIs.\n" +
            "Section 5.1: Using list\n" +
            "// Create a new read-only List<String>\n" +
            "val list = listOf(\"Item 1\", \"Item 2\", \"Item 3\")\n" +
            "println(list) // prints \"[Item 1, Item 2, Item 3]\"\n" +
            "Section 5.2: Using map\n" +
            "// Create a new read-only Map<Integer, String>\n" +
            "val map = mapOf(Pair(1, \"Item 1\"), Pair(2, \"Item 2\"), Pair(3, \"Item 3\"))\n" +
            "println(map) // prints \"{1=Item 1, 2=Item 2, 3=Item 3}\"\n" +
            "Section 5.3: Using set\n" +
            "// Create a new read-only Set<String>\n" +
            "val set = setOf(1, 3, 5)\n" +
            "println(set) // prints \"[1, 3, 5]\"\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 11\n" +
            "Chapter 6: Enum\n" +
            "Section 6.1: Initialization\n" +
            "Enum classes as any other classes can have a constructor and be initialized\n" +
            "enum class Color(val rgb: Int) {\n" +
            " RED(0xFF0000),\n" +
            " GREEN(0x00FF00),\n" +
            " BLUE(0x0000FF)\n" +
            "}\n" +
            "Section 6.2: Functions and Properties in enums\n" +
            "Enum classes can also declare members (i.e. properties and functions). A semicolon (;) must be placed between the\n" +
            "last enum object and the first member declaration.\n" +
            "If a member is abstract, the enum objects must implement it.\n" +
            "enum class Color {\n" +
            " RED {\n" +
            " override val rgb: Int = 0xFF0000\n" +
            " },\n" +
            " GREEN {\n" +
            " override val rgb: Int = 0x00FF00\n" +
            " },\n" +
            " BLUE {\n" +
            " override val rgb: Int = 0x0000FF\n" +
            " }\n" +
            " ;\n" +
            " abstract val rgb: Int\n" +
            " fun colorString() = \"#%06X\".format(0xFFFFFF and rgb)\n" +
            "}\n" +
            "Section 6.3: Simple enum\n" +
            "enum class Color {\n" +
            " RED, GREEN, BLUE\n" +
            "}\n" +
            "Each enum constant is an object. Enum constants are separated with commas.\n" +
            "Section 6.4: Mutability\n" +
            "Enums can be mutable, this is another way to obtain a singleton behavior:\n" +
            "enum class Planet(var population: Int = 0) {\n" +
            " EARTH(7 * 100000000),\n" +
            " MARS();\n" +
            " override fun toString() = \"$name[population=$population]\"\n" +
            "}\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 12\n" +
            " println(Planet.MARS) // MARS[population=0]\n" +
            " Planet.MARS.population = 3\n" +
            " println(Planet.MARS) // MARS[population=3]";
    static  String readingcmd="The arguments passed from the console can be received in the Kotlin program and it can be used as an input. You\n" +
            "can pass N (1 2 3 and so on) numbers of arguments from the command prompt.\n" +
            "A simple example of a command-line argument in Kotlin.\n" +
            "fun main(args: Array<String>) {\n" +
            " println(\"Enter Two number\")\n" +
            " var (a, b) = readLine()!!.split(\\'\\') // !! this operator use for NPE(NullPointerException).\n" +
            " println(\"Max number is : ${maxNum(a.toInt(), b.toInt())}\")\n" +
            "}\n" +
            "fun maxNum(a: Int, b: Int): Int {\n" +
            " var max = if (a > b) {\n" +
            " println(\"The value of a is $a\");\n" +
            " a\n" +
            " } else {\n" +
            " println(\"The value of b is $b\")\n" +
            " b\n" +
            " }\n" +
            " return max;\n" +
            "}\n" +
            "Here, Enter two number from the command line to find the maximum number. Output:\n" +
            "Enter Two number\n" +
            "71 89 // Enter two number from command line\n" +
            "The value of b is 89\n" +
            "Max number is: 89\n" +
            "For !! Operator Please check Null Safety.\n" +
            "Note: Above example compile and run on Intellij.";
    static  String basics="";
    static  String string="In Kotlin strings are compared with == operator which check for their structural equality.\n" +
            "val str1 = \"Hello, World!\"\n" +
            "val str2 = \"Hello,\" + \" World!\"\n" +
            "println(str1 == str2) // Prints true\n" +
            "Referential equality is checked with === operator.\n" +
            "val str1 = \"\"\"\n" +
            " |Hello, World!\n" +
            " \"\"\".trimMargin()\n" +
            "val str2 = \"\"\"\n" +
            " #Hello, World!\n" +
            " \"\"\".trimMargin(\"#\")\n" +
            "val str3 = str1\n" +
            "println(str1 == str2) // Prints true\n" +
            "println(str1 === str2) // Prints false\n" +
            "println(str1 === str3) // Prints true\n" +
            "Section 3.2: String Literals\n" +
            "Kotlin has two types of string literals:\n" +
            "Escaped string\n" +
            "Raw string\n" +
            "Escaped string handles special characters by escaping them. Escaping is done with a backslash. The following\n" +
            "escape sequences are supported: \\t, \\b, \\n, \\r, \\', \\\", \\\\ and \\$. To encode any other character, use the Unicode\n" +
            "escape sequence syntax: \\uFF00.\n" +
            "val s = \"Hello, world!\\n\"\n" +
            "Raw string delimited by a triple quote \"\"\", contains no escaping and can contain newlines and any other\n" +
            "characters\n" +
            "val text = \"\"\"\n" +
            " for (c in \"foo\")\n" +
            " print(c)\n" +
            "\"\"\"\n" +
            "Leading whitespace can be removed with trimMargin() function.\n" +
            "val text = \"\"\"\n" +
            " |Tell me and I forget.\n" +
            " |Teach me and I remember.\n" +
            " |Involve me and I learn.\n" +
            " |(Benjamin Franklin)\n" +
            " \"\"\".trimMargin()\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 7\n" +
            "Default margin prefix is pipe character |, this can be set as a parameter to trimMargin; e.g. trimMargin(\">\").\n" +
            "Section 3.3: Elements of String\n" +
            "Elements of String are characters that can be accessed by the indexing operation string[index].\n" +
            "val str = \"Hello, World!\"\n" +
            "println(str[1]) // Prints e\n" +
            "String elements can be iterated with a for-loop.\n" +
            "for (c in str) {\n" +
            " println(c)\n" +
            "}\n" +
            "Section 3.4: String Templates\n" +
            "Both escaped strings and raw strings can contain template expressions. Template expression is a piece of code\n" +
            "which is evaluated and its result is concatenated into string. It starts with a dollar sign $ and consists of either a\n" +
            "variable name:\n" +
            "val i = 10\n" +
            "val s = \"i = $i\" // evaluates to \"i = 10\"\n" +
            "Or an arbitrary expression in curly braces:\n" +
            "val s = \"abc\"\n" +
            "val str = \"$s.length is ${s.length}\" // evaluates to \"abc.length is 3\"\n" +
            "To include a literal dollar sign in a string, escape it using a backslash:\n" +
            "val str = \"\\$foo\" // evaluates to \"$foo\"\n" +
            "The exception is raw strings, which do not support escaping. In raw strings you can use the following syntax to\n" +
            "represent a dollar sign.\n" +
            "val price = \"\"\"\n" +
            "${'$'}9.99\n" +
            "\"\"\"";
    static String function="Parameter Details\n" +
            "Name Name of the function\n" +
            "Params Values given to the function with a name and type: Name:Type\n" +
            "Type Return type of the function\n" +
            "Type Argument Type parameter used in generic programming (not necessarily return type)\n" +
            "ArgName Name of value given to the function\n" +
            "ArgType Type specifier for ArgName\n" +
            "ArgNames List of ArgName separated by commas\n" +
            "Section 7.1: Function References\n" +
            "We can reference a function without actually calling it by prefixing the function's name with ::. This can then be\n" +
            "passed to a function which accepts some other function as a parameter.\n" +
            "fun addTwo(x: Int) = x + 2\n" +
            "listOf(1, 2, 3, 4).map(::addTwo) # => [3, 4, 5, 6]\n" +
            "Functions without a receiver will be converted to (ParamTypeA, ParamTypeB, ...) -> ReturnType where\n" +
            "ParamTypeA, ParamTypeB ... are the type of the function parameters and `ReturnType1 is the type of function return\n" +
            "value.\n" +
            "fun foo(p0: Foo0, p1: Foo1, p2: Foo2): Bar {\n" +
            " //...\n" +
            "}\n" +
            "println(::foo::class.java.genericInterfaces[0])\n" +
            "// kotlin.jvm.functions.Function3<Foo0, Foo1, Foo2, Bar>\n" +
            "// Human readable type: (Foo0, Foo1, Foo2) -> Bar\n" +
            "Functions with a receiver (be it an extension function or a member function) has a different syntax. You have to add\n" +
            "the type name of the receiver before the double colon:\n" +
            "class Foo\n" +
            "fun Foo.foo(p0: Foo0, p1: Foo1, p2: Foo2): Bar {\n" +
            " //...\n" +
            "}\n" +
            "val ref = Foo::foo\n" +
            "println(ref::class.java.genericInterfaces[0])\n" +
            "// kotlin.jvm.functions.Function4<Foo, Foo0, Foo1, Foo2, Bar>\n" +
            "// Human readable type: (Foo, Foo0, Foo1, Foo2) -> Bar\n" +
            "// takes 4 parameters, with receiver as first and actual parameters following, in their order\n" +
            "// this function can't be called like an extension function, though\n" +
            "val ref = Foo::foo\n" +
            "Foo().ref(Foo0(), Foo1(), Foo2()) // compile error\n" +
            "class Bar {\n" +
            " fun bar()\n" +
            "}\n" +
            "print(Bar::bar) // works on member functions, too.\n" +
            "However, when a function's receiver is an object, the receiver is omitted from parameter list, because these is and\n" +
            "only is one instance of such type.\n" +
            "object Foo\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 14\n" +
            "fun Foo.foo(p0: Foo0, p1: Foo1, p2: Foo2): Bar {\n" +
            " //...\n" +
            "}\n" +
            "val ref = Foo::foo\n" +
            "println(ref::class.java.genericInterfaces[0])\n" +
            "// kotlin.jvm.functions.Function3<Foo0, Foo1, Foo2, Bar>\n" +
            "// Human readable type: (Foo0, Foo1, Foo2) -> Bar\n" +
            "// takes 3 parameters, receiver not needed\n" +
            "object Bar {\n" +
            " fun bar()\n" +
            "}\n" +
            "print(Bar::bar) // works on member functions, too.\n" +
            "Since kotlin 1.1, function reference can also be bounded to a variable, which is then called a bounded function\n" +
            "reference.\n" +
            "Version ≥ 1.1.0\n" +
            "fun makeList(last: String?): List<String> {\n" +
            " val list = mutableListOf(\"a\", \"b\", \"c\")\n" +
            " last?.let(list::add)\n" +
            " return list\n" +
            "}\n" +
            "Note this example is given only to show how bounded function reference works. It's bad practice in all other\n" +
            "senses.\n" +
            "There is a special case, though. An extension function declared as a member can't be referenced.\n" +
            "class Foo\n" +
            "class Bar {\n" +
            " fun Foo.foo() {}\n" +
            " val ref = Foo::foo // compile error\n" +
            "}\n" +
            "Section 7.2: Basic Functions\n" +
            "Functions are declared using the fun keyword, followed by a function name and any parameters. You can also\n" +
            "specify the return type of a function, which defaults to Unit. The body of the function is enclosed in braces {}. If the\n" +
            "return type is other than Unit, the body must issue a return statement for every terminating branch within the\n" +
            "body.\n" +
            "fun sayMyName(name: String): String {\n" +
            " return \"Your name is $name\"\n" +
            "}\n" +
            "A shorthand version of the same:\n" +
            "fun sayMyName(name: String): String = \"Your name is $name\"\n" +
            "And the type can be omitted since it can be inferred:\n" +
            "fun sayMyName(name: String) = \"Your name is $name\"\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 15\n" +
            "Section 7.3: Inline Functions\n" +
            "Functions can be declared inline using the inline prefix, and in this case they act like macros in C - rather than\n" +
            "being called, they are replaced by the function's body code at compile time. This can lead to performance benefits\n" +
            "in some circumstances, mainly where lambdas are used as function parameters.\n" +
            "inline fun sayMyName(name: String) = \"Your name is $name\"\n" +
            "One difference from C macros is that inline functions can't access the scope from which they're called:\n" +
            "inline fun sayMyName() = \"Your name is $name\"\n" +
            "fun main() {\n" +
            " val name = \"Foo\"\n" +
            " sayMyName() # => Unresolved reference: name\n" +
            "}\n" +
            "Section 7.4: Lambda Functions\n" +
            "Lambda functions are anonymous functions which are usually created during a function call to act as a function\n" +
            "parameter. They are declared by surrounding expressions with {braces} - if arguments are needed, these are put\n" +
            "before an arrow ->.\n" +
            "{ name: String ->\n" +
            " \"Your name is $name\" //This is returned\n" +
            "}\n" +
            "The last statement inside a lambda function is automatically the return value.\n" +
            "The type's are optional, if you put the lambda on a place where the compiler can infer the types.\n" +
            "Multiple arguments:\n" +
            "{ argumentOne:String, argumentTwo:String ->\n" +
            " \"$argumentOne - $argumentTwo\"\n" +
            "}\n" +
            "If the lambda function only needs one argument, then the argument list can be omitted and the single argument be\n" +
            "referred to using it instead.\n" +
            "{ \"Your name is $it\" }\n" +
            "If the only argument to a function is a lambda function, then parentheses can be completely omitted from the\n" +
            "function call.\n" +
            "# These are identical\n" +
            "listOf(1, 2, 3, 4).map { it + 2 }\n" +
            "listOf(1, 2, 3, 4).map({ it + 2 })\n" +
            "Section 7.5: Operator functions\n" +
            "Kotlin allows us to provide implementations for a predefined set of operators with fixed symbolic representation\n" +
            "(like + or *) and fixed precedence. To implement an operator, we provide a member function or an extension\n" +
            "function with a fixed name, for the corresponding type. Functions that overload operators need to be marked with\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 16\n" +
            "the operator modifier:\n" +
            "data class IntListWrapper (val wrapped: List<Int>) {\n" +
            " operator fun get(position: Int): Int = wrapped[position]\n" +
            "}\n" +
            "val a = IntListWrapper(listOf(1, 2, 3))\n" +
            "a[1] // == 2\n" +
            "More operator functions can be found in here\n" +
            "Section 7.6: Functions Taking Other Functions\n" +
            "As seen in \"Lambda Functions\", functions can take other functions as a parameter. The \"function type\" which you'll\n" +
            "need to declare functions which take other functions is as follows:\n" +
            "# Takes no parameters and returns anything\n" +
            "() -> Any?\n" +
            "# Takes a string and an integer and returns ReturnType\n" +
            "(arg1: String, arg2: Int) -> ReturnType\n" +
            "For example, you could use the vaguest type, () -> Any?, to declare a function which executes a lambda function\n" +
            "twice:\n" +
            "fun twice(x: () -> Any?) {\n" +
            " x(); x();\n" +
            "}\n" +
            "fun main() {\n" +
            " twice {\n" +
            " println(\"Foo\")\n" +
            " } # => Foo\n" +
            " # => Foo\n" +
            "}\n" +
            "Section 7.7: Shorthand Functions\n" +
            "If a function contains just one expression, we can omit the brace brackets and use an equals instead, like a variable\n" +
            "assignment. The result of the expression is returned automatically.\n" +
            "fun sayMyName(name: String): String = \"Your name is $name\"\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 17\n" +
            "Chapter 8: Vararg Parameters in Functions\n" +
            "Section 8.1: Basics: Using the vararg keyword\n" +
            "Define the function using the vararg keyword.\n" +
            "fun printNumbers(vararg numbers: Int) {\n" +
            " for (number in numbers) {\n" +
            " println(number)\n" +
            " }\n" +
            "}\n" +
            "Now you can pass as many parameters (of the correct type) into the function as you want.\n" +
            "printNumbers(0, 1) // Prints \"0\" \"1\"\n" +
            "printNumbers(10, 20, 30, 500) // Prints \"10\" \"20\" \"30\" \"500\"\n" +
            "Notes: Vararg parameters must be the last parameter in the parameter list.\n" +
            "Section 8.2: Spread Operator: Passing arrays into vararg\n" +
            "functions\n" +
            "Arrays can be passed into vararg functions using the Spread Operator, *.\n" +
            "Assuming the following function exists...\n" +
            "fun printNumbers(vararg numbers: Int) {\n" +
            " for (number in numbers) {\n" +
            " println(number)\n" +
            " }\n" +
            "}\n" +
            "You can pass an array into the function like so...\n" +
            "val numbers = intArrayOf(1, 2, 3)\n" +
            "printNumbers(*numbers)\n" +
            "// This is the same as passing in (1, 2, 3)\n" +
            "The spread operator can also be used in the middle of the parameters...\n" +
            "val numbers = intArrayOf(1, 2, 3)\n" +
            "printNumbers(10, 20, *numbers, 30, 40)\n" +
            "// This is the same as passing in (10, 20, 1, 2, 3, 30, 40)";
    static String conditionalstatement="When given an argument, the when-statement matches the argument against the branches in sequence. The\n" +
            "matching is done using the == operator which performs null checks and compares the operands using the equals\n" +
            "function. The first matching one will be executed.\n" +
            "when (x) {\n" +
            " \"English\" -> print(\"How are you?\")\n" +
            " \"German\" -> print(\"Wie geht es dir?\")\n" +
            " else -> print(\"I don't know that language yet :(\")\n" +
            "}\n" +
            "The when statement also knows some more advanced matching options:\n" +
            "val names = listOf(\"John\", \"Sarah\", \"Tim\", \"Maggie\")\n" +
            "when (x) {\n" +
            " in names -> print(\"I know that name!\")\n" +
            " !in 1..10 -> print(\"Argument was not in the range from 1 to 10\")\n" +
            " is String -> print(x.length) // Due to smart casting, you can use String-functions here\n" +
            "}\n" +
            "Section 9.2: When-statement as expression\n" +
            "Like if, when can also be used as an expression:\n" +
            "val greeting = when (x) {\n" +
            " \"English\" -> \"How are you?\"\n" +
            " \"German\" -> \"Wie geht es dir?\"\n" +
            " else -> \"I don't know that language yet :(\"\n" +
            "}\n" +
            "print(greeting)\n" +
            "To be used as an expression, the when-statement must be exhaustive, i.e. either have an else branch or cover all\n" +
            "possibilities with the branches in another way.\n" +
            "Section 9.3: Standard if-statement\n" +
            "val str = \"Hello!\"\n" +
            "if (str.length == 0) {\n" +
            " print(\"The string is empty!\")\n" +
            "} else if (str.length > 5) {\n" +
            " print(\"The string is short!\")\n" +
            "} else {\n" +
            " print(\"The string is long!\")\n" +
            "}\n" +
            "The else-branches are optional in normal if-statements.\n" +
            "Section 9.4: If-statement as an expression\n" +
            "If-statements can be expressions:\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 19\n" +
            "val str = if (condition) \"Condition met!\" else \"Condition not met!\"\n" +
            "Note that the else-branch is not optional if the if-statement is used as an expression.\n" +
            "This can also been done with a multi-line variant with curly brackets and multiple else if statements.\n" +
            "val str = if (condition1){\n" +
            " \"Condition1 met!\"\n" +
            " } else if (condition2) {\n" +
            " \"Condition2 met!\"\n" +
            " } else {\n" +
            " \"Conditions not met!\"\n" +
            " }\n" +
            "TIP: Kotlin can infer the type of the variable for you but if you want to be sure of the type just annotate it\n" +
            "on the variable like: val str: String = this will enforce the type and will make it easier to read.\n" +
            "Section 9.5: When-statement instead of if-else-if chains\n" +
            "The when-statement is an alternative to an if-statement with multiple else-if-branches:\n" +
            "when {\n" +
            " str.length == 0 -> print(\"The string is empty!\")\n" +
            " str.length > 5 -> print(\"The string is short!\")\n" +
            " else -> print(\"The string is long!\")\n" +
            "}\n" +
            "Same code written using an if-else-if chain:\n" +
            "if (str.length == 0) {\n" +
            " print(\"The string is empty!\")\n" +
            "} else if (str.length > 5) {\n" +
            " print(\"The string is short!\")\n" +
            "} else {\n" +
            " print(\"The string is long!\")\n" +
            "}\n" +
            "Just like with the if-statement, the else-branch is optional, and you can add as many or as few branches as you like.\n" +
            "You can also have multiline-branches:\n" +
            "when {\n" +
            " condition -> {\n" +
            " doSomething()\n" +
            " doSomeMore()\n" +
            " }\n" +
            " else -> doSomethingElse()\n" +
            "}\n" +
            "Section 9.6: When-statement with enums\n" +
            "when can be used to match enum values:\n" +
            "enum class Day {\n" +
            " Sunday,\n" +
            " Monday,\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 20\n" +
            " Tuesday,\n" +
            " Wednesday,\n" +
            " Thursday,\n" +
            " Friday,\n" +
            " Saturday\n" +
            "}\n" +
            "fun doOnDay(day: Day) {\n" +
            " when(day) {\n" +
            " Day.Sunday -> // Do something\n" +
            " Day.Monday, Day.Tuesday -> // Do other thing\n" +
            " Day.Wednesday -> // ...\n" +
            " Day.Thursday -> // ...\n" +
            " Day.Friday -> // ...\n" +
            " Day.Saturday -> // ...\n" +
            " }\n" +
            "}\n" +
            "As you can see in second case line (Monday and Tuesday) it is also possible to combine two or more enum values.\n" +
            "If your cases are not exhaustive the compile will show an error. You can use else to handle default cases:\n" +
            "fun doOnDay(day: Day) {\n" +
            " when(day) {\n" +
            " Day.Monday -> // Work\n" +
            " Day.Tuesday -> // Work hard\n" +
            " Day.Wednesday -> // ...\n" +
            " Day.Thursday -> //\n" +
            " Day.Friday -> //\n" +
            " else -> // Party on weekend\n" +
            " }\n" +
            "}\n" +
            "Though the same can be done using if-then-else construct, when takes care of missing enum values and makes it\n" +
            "more natural.\n" +
            "Check here for more information about kotlin enum";
    static String loops="Section 10.1: Looping over iterables\n" +
            "You can loop over any iterable by using the standard for-loop:\n" +
            "val list = listOf(\"Hello\", \"World\", \"!\")\n" +
            "for(str in list) {\n" +
            " print(str)\n" +
            "}\n" +
            "Lots of things in Kotlin are iterable, like number ranges:\n" +
            "for(i in 0..9) {\n" +
            " print(i)\n" +
            "}\n" +
            "If you need an index while iterating:\n" +
            "for((index, element) in iterable.withIndex()) {\n" +
            " print(\"$element at index $index\")\n" +
            "}\n" +
            "There is also a functional approach to iterating included in the standard library, without apparent language\n" +
            "constructs, using the forEach function:\n" +
            "iterable.forEach {\n" +
            " print(it.toString())\n" +
            "}\n" +
            "it in this example implicitly holds the current element, see Lambda Functions\n" +
            "Section 10.2: Repeat an action x times\n" +
            "repeat(10) { i ->\n" +
            " println(\"This line will be printed 10 times\")\n" +
            " println(\"We are on the ${i + 1}. loop iteration\")\n" +
            "}\n" +
            "Section 10.3: Break and continue\n" +
            "Break and continue keywords work like they do in other languages.\n" +
            "while(true) {\n" +
            " if(condition1) {\n" +
            " continue // Will immediately start the next iteration, without executing the rest of the\n" +
            "loop body\n" +
            " }\n" +
            " if(condition2) {\n" +
            " break // Will exit the loop completely\n" +
            " }\n" +
            "}\n" +
            "If you have nested loops, you can label the loop statements and qualify the break and continue statements to\n" +
            "specify which loop you want to continue or break:\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 22\n" +
            "outer@ for(i in 0..10) {\n" +
            " inner@ for(j in 0..10) {\n" +
            " break // Will break the inner loop\n" +
            " break@inner // Will break the inner loop\n" +
            " break@outer // Will break the outer loop\n" +
            " }\n" +
            "}\n" +
            "This approach won't work for the functional forEach construct, though.\n" +
            "Section 10.4: Iterating over a Map in kotlin\n" +
            "//iterates over a map, getting the key and value at once\n" +
            "var map = hashMapOf(1 to \"foo\", 2 to \"bar\", 3 to \"baz\")\n" +
            "for ((key, value) in map) {\n" +
            " println(\"Map[$key] = $value\")\n" +
            "}\n" +
            "Section 10.5: Recursion\n" +
            "Looping via recursion is also possible in Kotlin as in most programming languages.\n" +
            "fun factorial(n: Long): Long = if (n == 0) 1 else n * factorial(n - 1)\n" +
            "println(factorial(10)) // 3628800\n" +
            "In the example above, the factorial function will be called repeatedly by itself until the given condition is met.\n" +
            "Section 10.6: While Loops\n" +
            "While and do-while loops work like they do in other languages:\n" +
            "while(condition) {\n" +
            " doSomething()\n" +
            "}\n" +
            "do {\n" +
            " doSomething()\n" +
            "} while (condition)\n" +
            "In the do-while loop, the condition block has access to values and variables declared in the loop body.\n" +
            "Section 10.7: Functional constructs for iteration\n" +
            "The Kotlin Standard Library also provides numerous useful functions to iteratively work upon collections.\n" +
            "For example, the map function can be used to transform a list of items.\n" +
            "val numbers = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 0)\n" +
            "val numberStrings = numbers.map { \"Number $it\" }\n" +
            "One of the many advantages of this style is it allows to chain operations in a similar fashion. Only a minor\n" +
            "modification would be required if say, the list above were needed to be filtered for even numbers. The filter\n" +
            "function can be used.\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 23\n" +
            "val numbers = listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 0)\n" +
            "val numberStrings = numbers.filter { it % 2 == 0 }.map { \"Number $it\" }";
    static String recyclerview="I just want to share my little bit knowledge and code of RecyclerView using Kotlin.\n" +
            "Section 30.1: Main class and Adapter\n" +
            "I am assuming that you have aware about the some syntax of Kotlin and how to use, just add RecyclerView in\n" +
            "activity_main.xml file and set with adapter class.\n" +
            "class MainActivity : AppCompatActivity(){\n" +
            "\n" +
            " lateinit var mRecyclerView : RecyclerView\n" +
            " val mAdapter : RecyclerAdapter = RecyclerAdapter()\n" +
            "\n" +
            " override fun onCreate(savedInstanceState: Bundle?) {\n" +
            " super.onCreate(savedInstanceState)\n" +
            " setContentView(R.layout.activity_main)\n" +
            " val toolbar = findViewById(R.id.toolbar) as Toolbar\n" +
            " setSupportActionBar(toolbar)\n" +
            "\n" +
            " mRecyclerView = findViewById(R.id.recycler_view) as RecyclerView\n" +
            "\n" +
            " mRecyclerView.setHasFixedSize(true)\n" +
            " mRecyclerView.layoutManager = LinearLayoutManager(this)\n" +
            " mAdapter.RecyclerAdapter(getList(), this)\n" +
            " mRecyclerView.adapter = mAdapter\n" +
            " }\n" +
            "\n" +
            " private fun getList(): ArrayList<String> {\n" +
            " var list : ArrayList<String> = ArrayList()\n" +
            " for (i in 1..10) { // equivalent of 1 <= i && i <= 10\n" +
            " println(i)\n" +
            " list.add(\"$i\")\n" +
            " }\n" +
            " return list\n" +
            " }\n" +
            " }\n" +
            "this one is your recycler view adapter class and create main_item.xml file what you want\n" +
            "class RecyclerAdapter : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {\n" +
            " var mItems: ArrayList<String> = ArrayList()\n" +
            " lateinit var mClick : OnClick\n" +
            " fun RecyclerAdapter(item : ArrayList<String>, mClick : OnClick){\n" +
            " this.mItems = item\n" +
            " this.mClick = mClick;\n" +
            " }\n" +
            " override fun onBindViewHolder(holder: ViewHolder, position: Int) {\n" +
            " val item = mItems[position]\n" +
            " holder.bind(item, mClick, position)\n" +
            " }\n" +
            " override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {\n" +
            " val layoutInflater = LayoutInflater.from(parent.context)\n" +
            " return ViewHolder(layoutInflater.inflate(R.layout.main_item, parent, false))\n" +
            " }\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 62\n" +
            " override fun getItemCount(): Int {\n" +
            " return mItems.size\n" +
            " }\n" +
            " class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {\n" +
            " val card = view.findViewById(R.id.card) as TextView\n" +
            " fun bind(str: String, mClick: OnClick, position: Int){\n" +
            " card.text = str\n" +
            " card.setOnClickListener { view ->\n" +
            " mClick.onClickListner(position)\n" +
            " }\n" +
            " }\n" +
            " }\n" +
            "}";
    String nullsafety="If the compiler can infer that an object can't be null at a certain point, you don't have to use the special operators\n" +
            "anymore:\n" +
            "var string: String? = \"Hello!\"\n" +
            "print(string.length) // Compile error\n" +
            "if(string != null) {\n" +
            " // The compiler now knows that string can't be null\n" +
            " print(string.length) // It works now!\n" +
            "}\n" +
            "Note: The compiler won't allow you to smart cast mutable variables that could potentially be modified\n" +
            "between the null-check and the intended usage.\n" +
            "If a variable is accessible from outside the scope of the current block (because they are members of a\n" +
            "non-local object, for example), you need to create a new, local reference which you can then smart cast\n" +
            "and use.\n" +
            "Section 14.2: Assertion\n" +
            "!! suffixes ignore nullability and returns a non-null version of that type. KotlinNullPointerException will be\n" +
            "thrown if the object is a null.\n" +
            "val message: String? = null\n" +
            "println(message!!) //KotlinNullPointerException thrown, app crashes\n" +
            "Section 14.3: Eliminate nulls from an Iterable and array\n" +
            "Sometimes we need to change type from Collection<T?> to Collections<T>. In that case, filterNotNull is our\n" +
            "solution.\n" +
            "val a: List<Int?> = listOf(1, 2, 3, null)\n" +
            "val b: List<Int> = a.filterNotNull()\n" +
            "Section 14.4: Null Coalescing / Elvis Operator\n" +
            "Sometimes it is desirable to evaluate a nullable expression in an if-else fashion. The elvis operator, ?:, can be used\n" +
            "in Kotlin for such a situation.\n" +
            "For instance:\n" +
            "val value: String = data?.first() ?: \"Nothing here.\"\n" +
            "The expression above returns \"Nothing here\" if data?.first() or data itself yield a null value else the result of\n" +
            "data?.first().\n" +
            "It is also possible to throw exceptions using the same syntax to abort code execution.\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 31\n" +
            "val value: String = data?.second()\n" +
            " ?: throw IllegalArgumentException(\"Value can't be null!\")\n" +
            "Reminder: NullPointerExceptions can be thrown using the assertion operator (e.g. data!!.second()!!)\n" +
            "Section 14.5: Nullable and Non-Nullable types\n" +
            "Normal types, like String, are not nullable. To make them able to hold null values, you have to explicitly denote\n" +
            "that by putting a ? behind them: String?\n" +
            "var string : String = \"Hello World!\" var nullableString: String? = null string = nullableString // Compiler error: Can't\n" +
            "assign nullable to non-nullable type. nullableString = string // This will work however!\n" +
            "Section 14.6: Elvis Operator (?:)\n" +
            "In Kotlin, we can declare variable which can hold null reference. Suppose we have a nullable reference a, we can\n" +
            "say \"if a is not null, use it, otherwise use some non-null value x\"\n" +
            "var a: String? = \"Nullable String Value\"\n" +
            "Now, a can be null. So when we need to access value of a, then we need to perform safety check, whether it\n" +
            "contains value or not. We can perform this safety check by conventional if...else statement.\n" +
            "val b: Int = if (a != null) a.length else -1\n" +
            "But here comes advance operator Elvis(Operator Elvis : ?:). Above if...else can be expressed with the Elvis\n" +
            "operator as below:\n" +
            "val b = a?.length ?: -1\n" +
            "If the expression to the left of ?: (here : a?.length) is not null, the elvis operator returns it, otherwise it returns the\n" +
            "expression to the right (here: -1). Right-hand side expression is evaluated only if the left-hand side is null.\n" +
            "Section 14.7: Safe call operator\n" +
            "To access functions and properties of nullable types, you have to use special operators.\n" +
            "The first one, ?., gives you the property or function you're trying to access, or it gives you null if the object is null:\n" +
            "val string: String? = \"Hello World!\"\n" +
            "print(string.length) // Compile error: Can't directly access property of nullable type.\n" +
            "print(string?.length) // Will print the string's length, or \"null\" if the string is null.\n" +
            "Idiom: calling multiple methods on the same, null-checked object\n" +
            "An elegant way to call multiple methods of a null-checked object is using Kotlin's apply like this:\n" +
            "obj?.apply {\n" +
            " foo()\n" +
            " bar()\n" +
            "}\n" +
            "This will call foo and bar on obj (which is this in the apply block) only if obj is non-null, skipping the entire block\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 32\n" +
            "otherwise.\n" +
            "To bring a nullable variable into scope as a non-nullable reference without making it the implicit receiver of function\n" +
            "and property calls, you can use let instead of apply:\n" +
            "nullable?.let { notnull ->\n" +
            " notnull.foo()\n" +
            " notnull.bar()\n" +
            "}\n" +
            "notnull could be named anything, or even left out and used through the implicit lambda parameter it";
    String lamda="Section 13.1: Lambda as parameter to filter function\n" +
            "val allowedUsers = users.filter { it.age > MINIMUM_AGE }\n" +
            "Section 13.2: Lambda for benchmarking a function call\n" +
            "General-purpose stopwatch for timing how long a function takes to run:\n" +
            "object Benchmark {\n" +
            " fun realtime(body: () -> Unit): Duration {\n" +
            " val start = Instant.now()\n" +
            " try {\n" +
            " body()\n" +
            " } finally {\n" +
            " val end = Instant.now()\n" +
            " return Duration.between(start, end)\n" +
            " }\n" +
            " }\n" +
            "}\n" +
            "Usage:\n" +
            "val time = Benchmark.realtime({\n" +
            " // some long-running code goes here ...\n" +
            "})\n" +
            "println(\"Executed the code in $time\")\n" +
            "Section 13.3: Lambda passed as a variable\n" +
            "val isOfAllowedAge = { user: User -> user.age > MINIMUM_AGE }\n" +
            "val allowedUsers = users.filter(isOfAllowedAge)";
    String kotlininterface="An interface in Kotlin can have default implementations for functions:\n" +
            "interface MyInterface {\n" +
            " fun withImplementation() {\n" +
            " print(\"withImplementation() was called\")\n" +
            " }\n" +
            "}\n" +
            "Classes implementing such interfaces will be able to use those functions without reimplementing\n" +
            "class MyClass: MyInterface {\n" +
            " // No need to reimplement here\n" +
            "}\n" +
            "val instance = MyClass()\n" +
            "instance.withImplementation()\n" +
            "Properties\n" +
            "Default implementations also work for property getters and setters:\n" +
            "interface MyInterface2 {\n" +
            " val helloWorld\n" +
            " get() = \"Hello World!\"\n" +
            "}\n" +
            "Interface accessors implementations can't use backing fields\n" +
            "interface MyInterface3 {\n" +
            " // this property won't compile!\n" +
            " var helloWorld: Int\n" +
            " get() = field\n" +
            " set(value) { field = value }\n" +
            "}\n" +
            "Multiple implementations\n" +
            "When multiple interfaces implement the same function, or all of them define with one or more implementing, the\n" +
            "derived class needs to manually resolve proper call\n" +
            "interface A {\n" +
            " fun notImplemented()\n" +
            " fun implementedOnlyInA() { print(\"only A\") }\n" +
            " fun implementedInBoth() { print(\"both, A\") }\n" +
            " fun implementedInOne() { print(\"implemented in A\") }\n" +
            "}\n" +
            "interface B {\n" +
            " fun implementedInBoth() { print(\"both, B\") }\n" +
            " fun implementedInOne() // only defined\n" +
            "}\n" +
            "class MyClass: A, B {\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 40\n" +
            " override fun notImplemented() { print(\"Normal implementation\") }\n" +
            " // implementedOnlyInA() can by normally used in instances\n" +
            " // class needs to define how to use interface functions\n" +
            " override fun implementedInBoth() {\n" +
            " super<B>.implementedInBoth()\n" +
            " super<A>.implementedInBoth()\n" +
            " }\n" +
            " // even if there's only one implementation, there multiple definitions\n" +
            " override fun implementedInOne() {\n" +
            " super<A>.implementedInOne()\n" +
            " print(\"implementedInOne class implementation\")\n" +
            " }\n" +
            "}\n" +
            "Section 19.2: Properties in Interfaces\n" +
            "You can declare properties in interfaces. Since an interface cannot have state you can only declare a property as\n" +
            "abstract or by providing default implementation for the accessors.\n" +
            "interface MyInterface {\n" +
            " val property: Int // abstract\n" +
            " val propertyWithImplementation: String\n" +
            " get() = \"foo\"\n" +
            " fun foo() {\n" +
            " print(property)\n" +
            " }\n" +
            "}\n" +
            "class Child : MyInterface {\n" +
            " override val property: Int = 29\n" +
            "}\n" +
            "Section 19.3: super keyword\n" +
            "interface MyInterface {\n" +
            " fun funcOne() {\n" +
            " //optional body\n" +
            " print(\"Function with default implementation\")\n" +
            " }\n" +
            "}\n" +
            "If the method in the interface has its own default implementation, we can use super keyword to access it.\n" +
            "super.funcOne()\n" +
            "Section 19.4: Basic Interface\n" +
            "A Kotlin interface contains declarations of abstract methods, and default method implementations although they\n" +
            "cannot store state.\n" +
            "interface MyInterface {\n" +
            " fun bar()\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 41\n" +
            "}\n" +
            "This interface can now be implemented by a class as follows:\n" +
            "class Child : MyInterface {\n" +
            " override fun bar() {\n" +
            " print(\"bar() was called\")\n" +
            " }\n" +
            "}\n" +
            "Section 19.5: Conflicts when Implementing Multiple Interfaces\n" +
            "with Default Implementations\n" +
            "When implementing more than one interface that have methods of the same name that include default\n" +
            "implementations, it is ambiguous to the compiler which implementation should be used. In the case of a conflict,\n" +
            "the developer must override the conflicting method and provide a custom implementation. That implementation\n" +
            "may choose to delegate to the default implementations or not.\n" +
            "interface FirstTrait {\n" +
            " fun foo() { print(\"first\") }\n" +
            " fun bar()\n" +
            "}\n" +
            "interface SecondTrait {\n" +
            " fun foo() { print(\"second\") }\n" +
            " fun bar() { print(\"bar\") }\n" +
            "}\n" +
            "class ClassWithConflict : FirstTrait, SecondTrait {\n" +
            " override fun foo() {\n" +
            " super<FirstTrait>.foo() // delegate to the default implementation of FirstTrait\n" +
            " super<SecondTrait>.foo() // delegate to the default implementation of SecondTrait\n" +
            " }\n" +
            " // function bar() only has a default implementation in one interface and therefore is ok.\n" +
            "}";
    String annotation="When declaring an annotation, meta-info can be included using the following meta-annotations:\n" +
            "@Target: specifies the possible kinds of elements which can be annotated with the annotation (classes,\n" +
            "functions, properties, expressions etc.)\n" +
            "@Retention specifies whether the annotation is stored in the compiled class files and whether it's visible\n" +
            "through reflection at runtime (by default, both are true.)\n" +
            "@Repeatable allows using the same annotation on a single element multiple times.\n" +
            "@MustBeDocumented specifies that the annotation is part of the public API and should be included in the class\n" +
            "or method signature shown in the generated API documentation.\n" +
            "Example:\n" +
            "@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION,\n" +
            " AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.EXPRESSION)\n" +
            "@Retention(AnnotationRetention.SOURCE)\n" +
            "@MustBeDocumented\n" +
            "annotation class Fancy\n" +
            "Section 22.2: Declaring an annotation\n" +
            "Annotations are means of attaching metadata to code. To declare an annotation, put the annotation modifier in\n" +
            "front of a class:\n" +
            "annotation class Strippable\n" +
            "Annotations can have meta-annotations:\n" +
            " @Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION, AnnotationTarget.VALUE_PARAMETER,\n" +
            "AnnotationTarget.EXPRESSION)\n" +
            " annotation class Strippable\n" +
            "Annotations, like other classes, can have constructors:\n" +
            "annotation class Strippable(val importanceValue: Int)\n" +
            "But unlike other classes, is limited to the following types:\n" +
            "types that correspond to Java primitive types (Int, Long etc.);\n" +
            "strings\n" +
            "classes ( Foo:: class)\n" +
            "enums\n" +
            "other annotations\n" +
            "arrays of the types listed above";
    String reflection="Reflection is a language's ability to inspect code at runtime instead of compile time.\n" +
            "Section 26.1: Referencing a class\n" +
            "To obtain a reference to a KClass object representing some class use double colons:\n" +
            "val c1 = String::class\n" +
            "val c2 = MyClass::class\n" +
            "Section 26.2: Inter-operating with Java reflection\n" +
            "To obtain a Java's Class object from Kotlin's KClass use the .java extension property:\n" +
            "val stringKClass: KClass<String> = String::class\n" +
            "val c1: Class<String> = stringKClass.java\n" +
            "val c2: Class<MyClass> = MyClass::class.java\n" +
            "The latter example will be optimized by the compiler to not allocate an intermediate KClass instance.\n" +
            "Section 26.3: Referencing a function\n" +
            "Functions are first-class citizens in Kotlin. You can obtain a reference on it using double colons and then pass it to\n" +
            "another function:\n" +
            "fun isPositive(x: Int) = x > 0\n" +
            "val numbers = listOf(-2, -1, 0, 1, 2)\n" +
            "println(numbers.filter(::isPositive)) // [1, 2]\n" +
            "Section 26.4: Getting values of all properties of a class\n" +
            "Given Example class extending BaseExample class with some properties:\n" +
            "open class BaseExample(val baseField: String)\n" +
            "class Example(val field1: String, val field2: Int, baseField: String):\n" +
            " BaseExample(baseField) {\n" +
            "\n" +
            " val field3: String\n" +
            " get() = \"Property without backing field\"\n" +
            " val field4 by lazy { \"Delegated value\" }\n" +
            " private val privateField: String = \"Private value\"\n" +
            "}\n" +
            "One can get hold of all properties of a class:\n" +
            "val example = Example(field1 = \"abc\", field2 = 1, baseField = \"someText\")\n" +
            "example::class.memberProperties.forEach { member ->\n" +
            " println(\"${member.name} -> ${member.get(example)}\")\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 50\n" +
            "}\n" +
            "Running this code will cause an exception to be thrown. Property private val privateField is declared private\n" +
            "and calling member.get(example) on it will not succeed. One way to handle this it to filter out private properties. To\n" +
            "do that we have to check the visibility modifier of a property's Java getter. In case of private val the getter does\n" +
            "not exist so we can assume private access.\n" +
            "The helper function and it's usage might look like this:\n" +
            "fun isFieldAccessible(property: KProperty1<*, *>): Boolean {\n" +
            " return property.javaGetter?.modifiers?.let { !Modifier.isPrivate(it) } ?: false\n" +
            "}\n" +
            "val example = Example(field1 = \"abc\", field2 = 1, baseField = \"someText\")\n" +
            "example::class.memberProperties.filter { isFieldAccessible(it) }.forEach { member ->\n" +
            " println(\"${member.name} -> ${member.get(example)}\")\n" +
            "}\n" +
            "Another approach is to make private properties accessible using reflection:\n" +
            "example::class.memberProperties.forEach { member ->\n" +
            " member.isAccessible = true\n" +
            " println(\"${member.name} -> ${member.get(example)}\")\n" +
            "}\n" +
            "Section 26.5: Setting values of all properties of a class\n" +
            "As an example we want to set all string properties of a sample class\n" +
            "class TestClass {\n" +
            " val readOnlyProperty: String\n" +
            " get() = \"Read only!\"\n" +
            " var readWriteString = \"asd\"\n" +
            " var readWriteInt = 23\n" +
            " var readWriteBackedStringProperty: String = \"\"\n" +
            " get() = field + '5'\n" +
            " set(value) { field = value + '5' }\n" +
            " var readWriteBackedIntProperty: Int = 0\n" +
            " get() = field + 1\n" +
            " set(value) { field = value - 1 }\n" +
            " var delegatedProperty: Int by TestDelegate()\n" +
            " private var privateProperty = \"This should be private\"\n" +
            " private class TestDelegate {\n" +
            " private var backingField = 3\n" +
            " operator fun getValue(thisRef: Any?, prop: KProperty<*>): Int {\n" +
            " return backingField\n" +
            " }\n" +
            " operator fun setValue(thisRef: Any?, prop: KProperty<*>, value: Int) {\n" +
            " backingField += value\n" +
            " }\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 51\n" +
            " }\n" +
            "}\n" +
            "Getting mutable properties builds on getting all properties, filtering mutable properties by type. We also need to\n" +
            "check visibility, as reading private properties results in run time exception.\n" +
            "val instance = TestClass()\n" +
            "TestClass::class.memberProperties\n" +
            " .filter{ prop.visibility == KVisibility.PUBLIC }\n" +
            " .filterIsInstance<KMutableProperty<*>>()\n" +
            " .forEach { prop ->\n" +
            " System.out.println(\"${prop.name} -> ${prop.get(instance)\")\n" +
            " }\n" +
            "To set all String properties to \"Our Value\" we can additionally filter by the return type. Since Kotlin is based on\n" +
            "Java VM, Type Erasure is in effect, and thus Properties returning generic types such as List<String> will be the\n" +
            "same as List<Any>. Sadly reflection is not a golden bullet and there is no sensible way to avoid this, so you need to\n" +
            "watch out in your use-cases.\n" +
            "val instance = TestClass()\n" +
            "TestClass::class.memberProperties\n" +
            " .filter{ prop.visibility == KVisibility.PUBLIC }\n" +
            " // We only want strings\n" +
            " .filter{ it.returnType.isSubtypeOf(String::class.starProjectedType) }\n" +
            " .filterIsInstance<KMutableProperty<*>>()\n" +
            " .forEach { prop ->\n" +
            " // Instead of printing the property we set it to some value\n" +
            " prop.setter.call(instance, \"Our Value\")\n" +
            " }";
    static String range="Range expressions are formed with rangeTo functions that have the operator form .. which is complemented by in\n" +
            "and !in. Range is defined for any comparable type, but for integral primitive types it has an optimized\n" +
            "implementation\n" +
            "Section 11.1: Integral Type Ranges\n" +
            "Integral type ranges ( IntRange , LongRange , CharRange ) have an extra feature: they can be iterated over. The\n" +
            "compiler takes care of converting this analogously to Java's indexed for-loop, without extra overhead\n" +
            "for (i in 1..4) print(i) // prints \"1234\"\n" +
            "for (i in 4..1) print(i) // prints nothing\n" +
            "Section 11.2: downTo() function\n" +
            "if you want to iterate over numbers in reverse order? It's simple. You can use the downTo() function defined in the\n" +
            "standard library\n" +
            "for (i in 4 downTo 1) print(i) // prints \"4321\"\n" +
            "Section 11.3: step() function\n" +
            "Is it possible to iterate over numbers with arbitrary step, not equal to 1? Sure, the step() function will help you\n" +
            "for (i in 1..4 step 2) print(i) // prints \"13\"\n" +
            "for (i in 4 downTo 1 step 2) print(i) // prints \"42\"\n" +
            "Section 11.4: until function\n" +
            "To create a range which does not include its end element, you can use the until function:\n" +
            "for (i in 1 until 10) { // i in [1, 10), 10 is excluded\n" +
            "println(i)\n" +
            "}";
    static String regex="Using immutable locals:\n" +
            "Uses less horizontal space but more vertical space than the \"anonymous temporaries\" template. Preferable over\n" +
            "the \"anonymous temporaries\" template if the when expression is in a loop--in that case, regex definitions should be\n" +
            "placed outside the loop.\n" +
            "import kotlin.text.regex\n" +
            "var string = /* some string */\n" +
            "val regex1 = Regex( /* pattern */ )\n" +
            "val regex2 = Regex( /* pattern */ )\n" +
            "/* etc */\n" +
            "when {\n" +
            " regex1.matches(string) -> /* do stuff */\n" +
            " regex2.matches(string) -> /* do stuff */\n" +
            " /* etc */\n" +
            "}\n" +
            "Using anonymous temporaries:\n" +
            "Uses less vertical space but more horizontal space than the \"immutable locals\" template. Should not be used if then\n" +
            "when expression is in a loop.\n" +
            "import kotlin.text.regex\n" +
            "var string = /* some string */\n" +
            "when {\n" +
            " Regex( /* pattern */ ).matches(string) -> /* do stuff */\n" +
            " Regex( /* pattern */ ).matches(string) -> /* do stuff */\n" +
            " /* etc */\n" +
            "}\n" +
            "Using the visitor pattern:\n" +
            "Has the benefit of closely emulating the \"argument-ful\" when syntax. This is beneficial because it more clearly\n" +
            "indicates the argument of the when expression, and also precludes certain programmer mistakes that could arise\n" +
            "from having to repeat the when argument in every whenEntry. Either the \"immutable locals\" or the \"anonymous\n" +
            "temporaries\" template may be used with this implementation the visitor pattern.\n" +
            "import kotlin.text.regex\n" +
            "var string = /* some string */\n" +
            "when (RegexWhenArgument(string)) {\n" +
            " Regex( /* pattern */ ) -> /* do stuff */\n" +
            " Regex( /* pattern */ ) -> /* do stuff */\n" +
            " /* etc */\n" +
            "}\n" +
            "And the minimal definition of the wrapper class for the when expression argument:\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 26\n" +
            "class RegexWhenArgument (val whenArgument: CharSequence) {\n" +
            " operator fun equals(whenEntry: Regex) = whenEntry.matches(whenArgument)\n" +
            " override operator fun equals(whenEntry: Any?) = (whenArgument == whenEntry)\n" +
            "}\n" +
            "Section 12.2: Introduction to regular expressions in Kotlin\n" +
            "This post shows how to use most of the functions in the Regex class, work with null safely related to the Regex\n" +
            "functions, and how raw strings makes it easier to write and read regex patterns.\n" +
            "The RegEx class\n" +
            "To work with regular expressions in Kotlin, you need to use the Regex(pattern: String) class and invoke\n" +
            "functions like find(..) or replace(..) on that regex object.\n" +
            "An example on how to use the Regex class that returns true if the input string contains c or d:\n" +
            "val regex = Regex(pattern = \"c|d\")\n" +
            "val matched = regex.containsMatchIn(input = \"abc\") // matched: true\n" +
            "The essential thing to understand with all the Regex functions is that the result is based on matching the regex\n" +
            "pattern and the input string. Some of the functions requires a full match, while the rest requires only a partial\n" +
            "match. The containsMatchIn(..) function used in the example requires a partial match and is explained later in\n" +
            "this post.\n" +
            "Null safety with regular expressions\n" +
            "Both find(..) and matchEntire(..) will return a MatchResult? object. The ? character after MatchResult is\n" +
            "necessary for Kotlin to handle null safely.\n" +
            "An example that demonstrates how Kotlin handles null safely from a Regex function, when the find(..) function\n" +
            "returns null:\n" +
            "val matchResult =\n" +
            " Regex(\"c|d\").find(\"efg\") // matchResult: null\n" +
            "val a = matchResult?.value // a: null\n" +
            "val b = matchResult?.value.orEmpty() // b: \"\"\n" +
            "a?.toUpperCase() // Still needs question mark. => null\n" +
            "b.toUpperCase() // Accesses the function directly. => \"\"\n" +
            "With the orEmpty() function, b can't be null and the ? character is unnecessary when you call functions on b.\n" +
            "If you don't care about this safe handling of null values, Kotlin allows you to work with null values like in Java with\n" +
            "the !! characters:\n" +
            "a!!.toUpperCase() // => KotlinNullPointerException\n" +
            "Raw strings in regex patterns\n" +
            "Kotlin provides an improvement over Java with a raw string that makes it possible to write pure regex patterns\n" +
            "without double backslashes, that are necessary with a Java string. A raw string is represented with a triple quote:\n" +
            "\"\"\"\\d{3}-\\d{3}-\\d{4}\"\"\" // raw Kotlin string\n" +
            "\"\\\\d{3}-\\\\d{3}-\\\\d{4}\" // standard Java string\n" +
            "find(input: CharSequence, startIndex: Int): MatchResult?\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 27\n" +
            "The input string will be matched against the pattern in the Regex object. It returns a Matchresult? object with the\n" +
            "first matched text after the startIndex, or null if the pattern didn't match the input string. The result string is\n" +
            "retrieved from the MatchResult? object's value property. The startIndex parameter is optional with the default\n" +
            "value 0.\n" +
            "To extract the first valid phone number from a string with contact details:\n" +
            "val phoneNumber :String? = Regex(pattern = \"\"\"\\d{3}-\\d{3}-\\d{4}\"\"\")\n" +
            " .find(input = \"phone: 123-456-7890, e..\")?.value // phoneNumber: 123-456-7890\n" +
            "With no valid phone number in the input string, the variable phoneNumber will be null.\n" +
            "findAll(input: CharSequence, startIndex: Int): Sequence\n" +
            "Returns all the matches from the input string that matches the regex pattern.\n" +
            "To print out all numbers separated with space, from a text with letters and digits:\n" +
            "val matchedResults = Regex(pattern = \"\"\"\\d+\"\"\").findAll(input = \"ab12cd34ef\")\n" +
            "val result = StringBuilder()\n" +
            "for (matchedText in matchedResults) {\n" +
            " result.append(matchedText.value + \" \")\n" +
            "}\n" +
            "println(result) // => 12 34\n" +
            "The matchedResults variable is a sequence with MatchResult objects. With an input string without digits, the\n" +
            "findAll(..) function will return an empty sequence.\n" +
            "matchEntire(input: CharSequence): MatchResult?\n" +
            "If all the characters in the input string matches the regex pattern, a string equal to the input will be returned. Else,\n" +
            "null will be returned.\n" +
            "Returns the input string if the whole input string is a number:\n" +
            "val a = Regex(\"\"\"\\d+\"\"\").matchEntire(\"100\")?.value // a: 100\n" +
            "val b = Regex(\"\"\"\\d+\"\"\").matchEntire(\"100 dollars\")?.value // b: null\n" +
            "matches(input: CharSequence): Boolean\n" +
            "Returns true if the whole input string matches the regex pattern. False otherwise.\n" +
            "Tests if two strings contains only digits:\n" +
            "val regex = Regex(pattern = \"\"\"\\d+\"\"\")\n" +
            "regex.matches(input = \"50\") // => true\n" +
            "regex.matches(input = \"50 dollars\") // => false\n" +
            "containsMatchIn(input: CharSequence): Boolean\n" +
            "Returns true if part of the input string matches the regex pattern. False otherwise.\n" +
            "Test if two strings contains at least one digit:\n" +
            "Regex(\"\"\"\\d+\"\"\").containsMatchIn(\"50 dollars\") // => true\n" +
            "Regex(\"\"\"\\d+\"\"\").containsMatchIn(\"Fifty dollars\") // => false\n" +
            "split(input: CharSequence, limit: Int): List\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 28\n" +
            "Returns a new list without all the regex matches.\n" +
            "To return lists without digits:\n" +
            "val a = Regex(\"\"\"\\d+\"\"\").split(\"ab12cd34ef\") // a: [ab, cd, ef]\n" +
            "val b = Regex(\"\"\"\\d+\"\"\").split(\"This is a test\") // b: [This is a test]\n" +
            "There is one element in the list for each split. The first input string has three numbers. That results in a list with\n" +
            "three elements.\n" +
            "replace(input: CharSequence, replacement: String): String\n" +
            "Replaces all matches of the regex pattern in the input string with the replacement string.\n" +
            "To replace all digits in a string with an x:\n" +
            "val result = Regex(\"\"\"\\d+\"\"\").replace(\"ab12cd34ef\", \"x\") // result: abxcdxef";
    static String javakotlin="Most people coming to Kotlin do have a programming background in Java.\n" +
            "This topic collects examples comparing Java to Kotlin, highlighting the most important differences and those gems\n" +
            "Kotlin offers over Java.\n" +
            "Section 35.1: Declaring Variables\n" +
            "In Kotlin, variable declarations look a bit different than Java's:\n" +
            "val i : Int = 42\n" +
            "They start with either val or var, making the declaration final (\"value\") or variable.\n" +
            "The type is noted after the name, separated by a :\n" +
            "Thanks to Kotlin's type inference the explicit type declaration can be omitted if there is an assignment with a\n" +
            "type the compiler is able to unambiguously detect\n" +
            "Java Kotlin\n" +
            "int i = 42; var i = 42 (or var i : Int = 42)\n" +
            "final int i = 42; val i = 42\n" +
            "Section 35.2: Quick Facts\n" +
            "Kotlin does not need ; to end statements\n" +
            "Kotlin is null-safe\n" +
            "Kotlin is 100% Java interoperable\n" +
            "Kotlin has no primitives (but optimizes their object counterparts for the JVM, if possible)\n" +
            "Kotlin classes have properties, not fields\n" +
            "Kotlin offers data classes with auto-generated equals/hashCode methods and field accessors\n" +
            "Kotlin only has runtime Exceptions, no checked Exceptions\n" +
            "Kotlin has no new keyword. Creating objects is done just by calling the constructor like any other method.\n" +
            "Kotlin supports (limited) operator overloading. For example, accessing a value of a map can be written like:\n" +
            "val a = someMap[\"key\"]\n" +
            "Kotlin can not only be compiled to byte code for the JVM, but also into Java Script, enabling you to write both\n" +
            "backend and frontend code in Kotlin\n" +
            "Kotlin is fully compatible with Java 6, which is especially interesting in regards for support of (not so) old\n" +
            "Android devices\n" +
            "Kotlin is an officially supported language for Android development\n" +
            "Kotlin's collections have built-in distinction between mutable and immutable collections.\n" +
            "Kotlin supports Coroutines (experimental)\n" +
            "Section 35.3: Equality & Identity\n" +
            "Kotlin uses == for equality (that is, calls equals internally) and === for referential identity.\n" +
            "Java Kotlin\n" +
            "a.equals(b); a == b\n" +
            "a == b; a === b\n" +
            "a != b; a !== b\n" +
            "GoalKicker.com – Kotlin® Notes for Professionals 70\n" +
            "See: https://kotlinlang.org/docs/reference/equality.html\n" +
            "Section 35.4: IF, TRY and others are expressions, not\n" +
            "statements\n" +
            "In Kotlin, if, try and others are expressions (so they do return a value) rather than (void) statements.\n" +
            "So, for example, Kotlin does not have Java's ternary Elvis Operator, but you can write something like this:\n" +
            "val i = if (someBoolean) 33 else 42\n" +
            "Even more unfamiliar, but equally expressive, is the try expression:\n" +
            "val i = try {\n" +
            " Integer.parseInt(someString)\n" +
            "}\n" +
            "catch (ex : Exception)\n" +
            "{\n" +
            " 42\n" +
            "}";
    static  String exception="Catching exceptions in Kotlin looks very similar to Java\n" +
            "try {\n" +
            " doSomething()\n" +
            "}\n" +
            "catch(e: MyException) {\n" +
            " handle(e)\n" +
            "}\n" +
            "finally {\n" +
            " cleanup()\n" +
            "}\n" +
            "You can also catch multiple exceptions\n" +
            "try {\n" +
            " doSomething()\n" +
            "}\n" +
            "catch(e: FileSystemException) {\n" +
            " handle(e)\n" +
            "}\n" +
            "catch(e: NetworkException) {\n" +
            " handle(e)\n" +
            "}\n" +
            "catch(e: MemoryException) {\n" +
            " handle(e)\n" +
            "}\n" +
            "finally {\n" +
            " cleanup()\n" +
            "}\n" +
            "try is also an expression and may return value\n" +
            "val s: String? = try { getString() } catch (e: Exception) { null }\n" +
            "Kotlin doesn't have checked exceptions, so you don't have to catch any exceptions.\n" +
            "fun fileToString(file: File) : String {\n" +
            " //readAllBytes throws IOException, but we can omit catching it\n" +
            " fileContent = Files.readAllBytes(file)\n" +
            " return String(fileContent)\n" +
            "}";

}
