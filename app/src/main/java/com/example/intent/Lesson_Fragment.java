package com.example.intent;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Lesson_Fragment extends Fragment {
    Context basectx;
    RecyclerView recyclerView;
    List list;
    View view;
    public Lesson_Fragment(Context context) {
        // Required empty public constructor
        this.basectx=context;
        list=new ArrayList();
        list.add(new Lesson(1,"Hello World",Data.helloworld,"htttp://www.hello world"));
        list.add(new Lesson(2,"basics of kotlin",Data.basics,"htttp://www.hello world"));
        list.add(new Lesson(3,"reading from cmd",Data.readingcmd,"htttp://www.hello world"));
        list.add(new Lesson(4,"string literals",Data.string,"htttp://www.hello world"));
        list.add(new Lesson(5,"loops in kotlin",Data.loops,"htttp://www.hello world"));
        list.add(new Lesson(6,"conditional statement",Data.conditionalstatement,"htttp://www.hello world"));
        list.add(new Lesson(7,"Array Iteration in kotlin",Data.arrayIteration,"htttp://www.hello world"));
        list.add(new Lesson(8," basics of function in kotlin",Data.function,"htttp://www.hello world"));
        list.add(new Lesson(9,"exceptions in kotlin",Data.exception,"htttp://www.hello world"));
        list.add(new Lesson(10,"Collection in kotlin",Data.collection,"htttp://www.hello world"));
        list.add(new Lesson(11,"Range in kotlin",Data.range,"htttp://www.hello world"));
        list.add(new Lesson(12,"Regex in kotlin",Data.regex,"htttp://www.hello world"));
        list.add(new Lesson(13," how to use RecyclerView with kotlin",Data.recyclerview,"htttp://www.hello world"));
        list.add(new Lesson(14,"Kotlin and java interoperatability",Data.javakotlin,"htttp://www.hello world"));

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.activity_lesson, container, false);
        recyclerView=view.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        LessonAdapter lessonAdapter=new LessonAdapter(getContext(),list);
        recyclerView.setAdapter(lessonAdapter);
        return view;
    }

}
