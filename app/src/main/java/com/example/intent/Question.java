package com.example.intent;

public class Question {
    int qno;
    String question;
    String answer;
    String description;

    public Question(int qno, String question, String answer, String description) {
        this.qno = qno;
        this.question = question;
        this.answer = answer;
        this.description = description;
    }

    public int getQno() {
        return qno;
    }

    public void setQno(int qno) {
        this.qno = qno;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
