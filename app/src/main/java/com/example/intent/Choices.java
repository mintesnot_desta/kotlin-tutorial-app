package com.example.intent;

public class Choices {
    int qno=0;
    String a;
    String b;
    String c;
    String d;

    public Choices(int qno,String a, String b, String c, String d) {
        this.qno=qno;
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    public String getA() {
        return a;
    }
    public void setA(String a) {
        this.a = a;
    }
    public String getB() {
        return b;
    }
    public void setB(String b) {
        this.b = b;
    }
    public String getC() {
        return c;
    }

    public void setC(String c) {
        this.c = c;
    }
    public String getD() {
        return d;
    }
    public void setD(String d) {
        this.d = d;
    }

    public int getQno() {
        return qno;
    }

    public void setQno(int qno) {
        this.qno = qno;
    }
}
