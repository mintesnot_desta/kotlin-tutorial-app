package com.example.intent;

public class Lesson {
    int lesson;
    String title;
    String details;
    String links;

    public Lesson(int lesson, String title, String details, String links) {
        this.lesson = lesson;
        this.title = title;
        this.details = details;
        this.links = links;
    }

    public int getLesson() {
        return lesson;
    }

    public void setLesson(int lesson) {
        this.lesson = lesson;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getLinks() {
        return links;
    }

    public void setLinks(String links) {
        this.links = links;
    }
}
